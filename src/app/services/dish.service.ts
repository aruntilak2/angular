import { Injectable } from '@angular/core';
import { Dish} from '../shared/dish';
import { DISHES } from '../shared/dishes';
import { Observable } from 'rxjs/Observable';
//Removing the promoises to use observables
// import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
@Injectable()
export class DishService {

  constructor() { }

  // getDishes(): Promise< Dish[]>{
    //  return Promise.resolve(DISHES);
    // return new Promise( resolve => {
    //   //Simulate server latency with 2 seconds delay
    //   setTimeout(() => resolve (DISHES), 2000)
    // });
            // remving promises
            // getDishes(): Promise<Dish[]> {
            //   return Observable.of(DISHES).delay(2000).toPromise();
            // }
            // using obervables
              getDishes(): Observable<Dish[]> {
               return Observable.of(DISHES).delay(2000);
               }

  // getDish(id: number): Promise<Dish> {
  // return new Promise( resolve => {
  //   //Simulate server latency with 2 seconds delay
  //   setTimeout(() =>resolve (DISHES.filter((dish) => (dish.id === id))[0]),2000) ;
  // }); //Removing Promises
        // getDish(id: number): Promise<Dish> {
        //   return Observable.of(DISHES.filter((dish) => (dish.id === id))[0]).delay(2000).toPromise();
        // }
              getDish(id: number): Observable<Dish> {
                 return Observable.of(DISHES.filter((dish) => (dish.id === id))[0]).delay(2000);
                 }
        
//   getFeaturedDish():Promise<Dish>{
//     return new Promise( resolve => {
//       //Simulate server latency with 2 seconds delay
//       setTimeout(() => resolve (DISHES.filter((dish) => (dish.featured))[0]),2000);
//   });
// }
      //removing promises
      // getFeaturedDish(): Promise<Dish> {
      //   return Observable.of(DISHES.filter((dish) => dish.featured)[0]).delay(2000).toPromise();
      // }
              getFeaturedDish(): Observable<Dish> {
                return Observable.of(DISHES.filter((dish) => dish.featured)[0]).delay(2000);
              }
              getDishIds(): Observable<number[]> { 
                return Observable.of(DISHES.map(dish => dish.id)).delay(2000);
              }

}