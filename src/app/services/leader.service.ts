import { Injectable } from '@angular/core';
import { Leader } from '../shared/leader';
import { LEADERS } from '../shared/leaders';
import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/delay';
import 'rxjs/add/observable/of';
@Injectable()
export class LeaderService {

  constructor() { }

  // getLeaders(): Promise<Leader []>{
  //   // return Promise.resolve(LEADERS);
  //   return new Promise ( resolve => {
  //     //Simlutae server latency with 2 second delay
  //     setTimeout (() => resolve (LEADERS), 2000);
  //   });
  //Removing promises
  // getLeaders(): Promise<Leader[]> {
  //   return Observable.of(LEADERS).delay(2000).toPromise();
  // }
        getLeaders(): Observable<Leader[]> {
          return Observable.of(LEADERS).delay(2000);
        }
    
// getLeader(id: number): Promise<Leader> {
// return new Promise ( resolve => {
//   //Simlutae server latency with 2 second delay
//   setTimeout (() => resolve(LEADERS.filter((lead)=>(lead.id === id))[0]),2000);
// });
// REmoving promises
// getLeader(id: number): Promise<Leader> {
//   return Observable.of(LEADERS.filter((leader) => (leader.id === id))[0]).delay(2000).toPromise();
// }

        getLeader(id: number): Observable<Leader> {
          return Observable.of(LEADERS.filter((leader) => (leader.id === id))[0]).delay(2000);
        }


// getFeaturedLeader(): Promise<Leader>{
//   return new Promise( resolve => {
//     //Simlutae server latency with 2 second delay
//     setTimeout (() => resolve(LEADERS.filter((lead) => (lead.featured))[0]),2000);
// });
// getFeaturedLeader(): Promise<Leader> {
//   return Observable.of(LEADERS.filter((leader) => leader.featured)[0]).delay(2000).toPromise();
// }
      getFeaturedLeader(): Observable<Leader> {
          return Observable.of(LEADERS.filter((leader) => leader.featured)[0]).delay(2000);
        }
  
}